//
//  Common.h
//  Timetable
//
//  Created by khalil on 17/1/18.
//  Copyright © 2017年 com.Khalil. All rights reserved.
//

#define ScreenWidth  CGRectGetWidth([UIScreen mainScreen].bounds)
#define ScreenHeight CGRectGetHeight([UIScreen mainScreen].bounds)
#define SCREEN_SIZE [NSString stringWithFormat:@"%dx%d", (int)ScreenWidth, (int)ScreenHeight]

#define kStatusBarHeight [[UIApplication sharedApplication] statusBarFrame].size.height
#define kNavBarHeight [UINavigationBar appearance].frame.size.height
#define kTabBarHeight ([[UIApplication sharedApplication] statusBarFrame].size.height>20?83:49)
#define kTopHeight (kStatusBarHeight + kNavBarHeight)

#define API_SERVER @"https://shopicruit.myshopify.com"
#define ACCESS_TOKEN @"c32313df0d0ef512ca64d5b336a0d7c6"

#define INITIAL_PAGE 1
