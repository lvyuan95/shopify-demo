//
//  ProductVariant.h
//  ShopifyChallenger
//
//  Created by Khalil on 2019/1/18.
//  Copyright © 2019年 Khalil. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ProductVariant : NSObject

@property NSInteger objectId;
@property (nonatomic, strong) NSString *title;
@property NSInteger quantity;


+ (ProductVariant *)getDataFromDictionary:(NSDictionary *)data;

@end
