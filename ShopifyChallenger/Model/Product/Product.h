//
//  Product.h
//  ShopifyChallenger
//
//  Created by Khalil on 2019/1/18.
//  Copyright © 2019年 Khalil. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Product : NSObject

@property NSInteger objectId;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *imageURL;
@property (nonatomic, strong) NSArray *variants;
@property NSInteger totalInventory;


+ (Product *)getDataFromDictionary:(NSDictionary *)data;

@end
