//
//  Product.m
//  ShopifyChallenger
//
//  Created by Khalil on 2019/1/18.
//  Copyright © 2019年 Khalil. All rights reserved.
//

#import "Product.h"
#import "ProductVariant.h"

@implementation Product

+ (Product *)getDataFromDictionary:(NSDictionary *)data{
    if (data) {
        Product *product = [[Product alloc] init];
        product.objectId = [data[@"id"] integerValue];
        product.title = data[@"title"];
        product.imageURL = data[@"image"][@"src"];
        
        NSMutableArray *variants = [NSMutableArray arrayWithArray:@[]];
        product.totalInventory = 0;
        for (NSDictionary *item in data[@"variants"]){
            ProductVariant *variant = [ProductVariant getDataFromDictionary:item];
            [variants addObject:variant];
            product.totalInventory += variant.quantity;
        }
        product.variants = [NSArray arrayWithArray:variants];
        return product;
    }
    return nil;
}

@end
