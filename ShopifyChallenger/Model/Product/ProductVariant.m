//
//  ProductVariant.m
//  ShopifyChallenger
//
//  Created by Khalil on 2019/1/18.
//  Copyright © 2019年 Khalil. All rights reserved.
//

#import "ProductVariant.h"

@implementation ProductVariant

+ (ProductVariant *)getDataFromDictionary:(NSDictionary *)data{
    if (data) {
        ProductVariant *variant = [[ProductVariant alloc] init];
        variant.objectId = [data[@"id"] integerValue];
        variant.title = data[@"title"];
        variant.quantity = [data[@"inventory_quantity"] integerValue];
        return variant;
    }
    return nil;
}

@end
