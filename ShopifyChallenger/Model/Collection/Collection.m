//
//  Collection.m
//  ShopifyChallenger
//
//  Created by Khalil on 2019/1/18.
//  Copyright © 2019年 Khalil. All rights reserved.
//

#import "Collection.h"

@implementation Collection


+ (Collection *)getDataFromDictionary:(NSDictionary *)data{
    if (data) {
        Collection *collection = [[Collection alloc] init];
        collection.objectId = [data[@"id"] integerValue];
        collection.title = data[@"title"];
        collection.imageUrl = data[@"image"][@"src"];
        collection.bodyHTML = data[@"body_html"];
        return collection;
    }
    return nil;
}

@end
