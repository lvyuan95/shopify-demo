//
//  Collection.h
//  ShopifyChallenger
//
//  Created by Khalil on 2019/1/18.
//  Copyright © 2019年 Khalil. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Collection : NSObject


@property NSInteger objectId;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *imageUrl;
@property (nonatomic, strong) NSString *bodyHTML;


+ (Collection *)getDataFromDictionary:(NSDictionary *)data;

@end
