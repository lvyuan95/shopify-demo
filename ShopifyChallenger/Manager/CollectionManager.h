//
//  AppManager.h
//  Timetable
//
//  Created by khalil on 17/2/7.
//  Copyright © 2017年 com.Khalil. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CollectionManager : NSObject

+ (NSDictionary *)loadCollections: (NSInteger)page;
+ (NSDictionary *)loadProductsForCollection: (NSInteger)objectId page:(NSInteger)page;

+ (NSDictionary *)sendAPIRequest:(NSString *)type url:(NSString *)url parameter:(NSString *)parameter;
@end
