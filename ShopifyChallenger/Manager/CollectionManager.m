//
//  AppManager.m
//  Timetable
//
//  Created by khalil on 17/2/7.
//  Copyright © 2017年 com.Khalil. All rights reserved.
//

#import "Common.h"
#import "CJSONDeserializer.h"

@implementation CollectionManager : NSObject

+ (NSDictionary *)loadCollections: (NSInteger)page{
    NSString *stringData = [NSString stringWithFormat:@"page=%ld&access_token=%@", page, ACCESS_TOKEN];
    return [CollectionManager sendAPIRequest:@"GET" url:@"/admin/custom_collections.json" parameter:stringData];
}

+ (NSDictionary *)loadProductsForCollection: (NSInteger)objectId page:(NSInteger)page{
    NSString *stringData = [NSString stringWithFormat:@"collection_id=%ld&page=%ld&access_token=%@", (long)objectId, (long)page, ACCESS_TOKEN];
    NSDictionary *result = [CollectionManager sendAPIRequest:@"GET" url:@"/admin/collects.json" parameter:stringData];
    NSString * productIds = @"";
    // No more products
    if ([result[@"collects"] count] == 0){
        return @{@"products": @[]};
    }
    for (NSDictionary *item in result[@"collects"]){
        productIds = [NSString stringWithFormat:@"%@,%@", productIds, [item[@"product_id"] stringValue]];
    }
    productIds = [productIds substringFromIndex:1];
    NSString *productRequest = [NSString stringWithFormat:@"ids=%@&page=%ld&access_token=%@", productIds, (long)page, ACCESS_TOKEN];
    return [CollectionManager sendAPIRequest:@"GET" url:@"/admin/products.json" parameter:productRequest];
}


+ (NSDictionary *)sendAPIRequest:(NSString *)type url:(NSString *)url parameter:(NSString *)parameter{
    NSDictionary *result;
    
    NSString *requestURL = [API_SERVER stringByAppendingString:[NSString stringWithFormat:@"%@?%@", url, parameter]];
    
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:requestURL]
                                                  cachePolicy:NSURLRequestUseProtocolCachePolicy
                                              timeoutInterval:20.0f];
    NSError *error = nil;
    NSURLResponse *response = [[NSURLResponse alloc] init];
    
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request
                                                 returningResponse:&response
                                                             error:&error];
    
    result = [[CJSONDeserializer deserializer] deserialize:responseData error:&error];
    return result;
}

@end
