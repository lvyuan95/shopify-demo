//
//  CollectionCell.m
//  ShopifyChallenger
//
//  Created by Khalil on 2019/1/19.
//  Copyright © 2019年 Khalil. All rights reserved.
//

#import "CollectionCell.h"
#import <YYKit.h>

@implementation CollectionCell
@synthesize image, cellTitle;

- (void)awakeFromNib {
    [super awakeFromNib];
    
}

- (void)setCellData: (Collection *)collection{
    [self.image setImageWithURL:[NSURL URLWithString:collection.imageUrl] options:YYWebImageOptionProgressive];
    self.image.layer.cornerRadius = 6.0;
    self.image.clipsToBounds = YES;
    [self.image setContentMode:UIViewContentModeScaleAspectFill];
    
    
    self.cellTitle.text = collection.title;
    self.cellTitle.numberOfLines = 2;
    self.cellTitle.textColor = [UIColor whiteColor];
    self.cellTitle.layer.shadowRadius = 2;
    self.cellTitle.layer.shadowOpacity = 1;
    self.cellTitle.layer.shadowColor = [UIColor blackColor].CGColor;
    self.cellTitle.layer.shadowOffset = CGSizeMake(0.0, 1.0);
}
@end
