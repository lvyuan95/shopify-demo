//
//  CollectionCell.h
//  ShopifyChallenger
//
//  Created by Khalil on 2019/1/19.
//  Copyright © 2019年 Khalil. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Collection.h"

@interface CollectionCell : UICollectionViewCell

- (void)setCellData: (Collection *)collection;

@property (weak, nonatomic) IBOutlet UIImageView *image;
@property (weak, nonatomic) IBOutlet UILabel *cellTitle;

@end
