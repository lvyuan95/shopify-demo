
//
//  CollectionListViewController.m
//  ShopifyChallenger
//
//  Created by Khalil on 2019/1/18.
//  Copyright © 2019年 Khalil. All rights reserved.
//

#import "CollectionListViewController.h"
#import "Collection.h"
#import "CollectionCell.h"
#import "CollectionManager.h"
#import "Common.h"
#import "CollectionDetailViewController.h"

#import "MJRefresh.h"

@interface CollectionListViewController ()<UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>

@property (nonatomic, strong) NSMutableArray *collectionArray;
@property NSInteger currentPage;

@end

@implementation CollectionListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.collectionArray = [NSMutableArray arrayWithArray:@[]];
    self.currentPage = INITIAL_PAGE;
    
    [self setupSubview];
}

- (void)setupSubview {
    [self.collectionView setContentInset:UIEdgeInsetsMake(24, 0, 0, 0)];
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    [self.view addSubview:self.collectionView];
    
    self.title = @"Collections";
    
    self.collectionView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        self.currentPage = INITIAL_PAGE;
        [self loadData];
    }];
    
    MJRefreshAutoNormalFooter *footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadData)];
    footer.triggerAutomaticallyRefreshPercent = -10;
    [footer setRefreshingTitleHidden:YES];
    
    self.collectionView.mj_footer = footer;
    
    [self.collectionView.mj_header beginRefreshing];
}

- (void)loadData {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        NSDictionary *result = [CollectionManager loadCollections: self.currentPage];
        dispatch_async(dispatch_get_main_queue(), ^{
            if (result != nil && result[@"custom_collections"]){
                if ([result[@"custom_collections"] count] == 0){
                    [self.collectionView.mj_footer endRefreshingWithNoMoreData];
                    return;
                }
                NSMutableArray *array = [NSMutableArray arrayWithArray:@[]];
                for (NSDictionary *item in result[@"custom_collections"]){
                    [array addObject:[Collection getDataFromDictionary:item]];
                }
                if (self.currentPage == INITIAL_PAGE){
                    [self.collectionArray removeAllObjects];
                }
                [self.collectionArray addObjectsFromArray:array];
                self.currentPage += 1;
                [self.collectionView reloadData];
            }
            else{
                NSLog(@"Failed to load, omit");
            }
            [self.collectionView.mj_header endRefreshing];
            [self.collectionView.mj_footer endRefreshing];
        });
    });
}

- (void)openCollection: (Collection *)collection {
    CollectionDetailViewController *vc = [CollectionDetailViewController new];
    vc.selectedCollection = collection;
    [self.navigationController pushViewController:vc animated:YES];
}

# pragma mark - MJRefresh methods
- (void)headerRereshing{
    
}
- (void)footerRereshing{
    
}
# pragma mark - CollectionView delegates

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(ScreenWidth / 2 - 16, ScreenWidth / 2 - 16);
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return [self.collectionArray count];
}


- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    Collection *collection = self.collectionArray[indexPath.row];
    [self openCollection:collection];

}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"CollectionCell" forIndexPath:indexPath];
    Collection *collection = self.collectionArray[indexPath.row];
    [cell setCellData:collection];
    return cell;
}

- (UICollectionView *)collectionView
{
    if (_collectionView == nil) {
        UICollectionViewFlowLayout *flow = [[UICollectionViewFlowLayout alloc] init];
        
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, kTopHeight, ScreenWidth, ScreenHeight - kTopHeight) collectionViewLayout:flow];
        
        _collectionView.backgroundColor = [UIColor whiteColor];
        _collectionView.scrollsToTop = YES;
        
        
        UINib *cellNib = [UINib nibWithNibName:@"CollectionCell" bundle:nil];
        [_collectionView registerNib:cellNib forCellWithReuseIdentifier:@"CollectionCell"];
        
    }
    return _collectionView;
}


@end
