//
//  CollectionListViewController.h
//  ShopifyChallenger
//
//  Created by Khalil on 2019/1/18.
//  Copyright © 2019年 Khalil. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CollectionListViewController : UIViewController

@property (nonatomic, strong) UICollectionView *collectionView;

@end
