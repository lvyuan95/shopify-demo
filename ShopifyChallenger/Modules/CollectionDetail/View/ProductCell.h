//
//  ProductCell.h
//  ShopifyChallenger
//
//  Created by Khalil on 2019/1/19.
//  Copyright © 2019年 Khalil. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProductCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *cellTitle;
@property (weak, nonatomic) IBOutlet UILabel *cellSubtitle;
@property (weak, nonatomic) IBOutlet UIImageView *image;

- (void)setCellData: (NSString *)imageURL title:(NSString *)title subtitle:(NSString *)subtitle;
@end
