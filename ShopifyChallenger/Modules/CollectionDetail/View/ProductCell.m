//
//  ProductCell.m
//  ShopifyChallenger
//
//  Created by Khalil on 2019/1/19.
//  Copyright © 2019年 Khalil. All rights reserved.
//

#import "ProductCell.h"
#import <YYKit.h>

@implementation ProductCell
@synthesize cellTitle, cellSubtitle, image;

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setCellData: (NSString *)imageURL title:(NSString *)title subtitle:(NSString *)subtitle{
    [self.image setImageWithURL:[NSURL URLWithString:imageURL] options:YYWebImageOptionProgressive];
    self.image.layer.cornerRadius = 6.0;
    self.image.clipsToBounds = YES;
    [self.image setContentMode:UIViewContentModeScaleAspectFill];
    
    self.cellTitle.text = title;
    
    // If no description provided, display placeholder text to the flex height display
    self.cellSubtitle.text = [subtitle length] != 0 ? subtitle : @"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. ";
}

@end
