//
//  CollectionDetailViewController.m
//  ShopifyChallenger
//
//  Created by Khalil on 2019/1/18.
//  Copyright © 2019年 Khalil. All rights reserved.
//

#import "CollectionDetailViewController.h"
#import "Product.h"
#import "ProductCell.h"
#import "Common.h"
#import "CollectionManager.h"

#import "MJRefresh.h"

@interface CollectionDetailViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) NSMutableArray *productArray;
@property NSInteger currentPage;

@end

@implementation CollectionDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.productArray = [NSMutableArray arrayWithArray:@[]];
    self.currentPage = INITIAL_PAGE;
    
    [self setupSubview];
}

- (void)setupSubview {
    [self.tableView setContentInset:UIEdgeInsetsMake(64, 0, 0, 0)];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.view addSubview:self.tableView];
    
    self.title = @"Collection Detail";
    
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        self.currentPage = INITIAL_PAGE;
        [self loadProductData];
    }];
    
    MJRefreshAutoNormalFooter *footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadProductData)];
    footer.triggerAutomaticallyRefreshPercent = -10;
    [footer setRefreshingTitleHidden:YES];
    
    self.tableView.mj_footer = footer;
    
    [self.tableView.mj_header beginRefreshing];
}

- (void)loadProductData{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        NSDictionary *result = [CollectionManager loadProductsForCollection:self.selectedCollection.objectId page:self.currentPage];
        dispatch_async(dispatch_get_main_queue(), ^{
            if (result != nil && result[@"products"]){
                if ([result[@"products"] count] == 0){
                    [self.tableView.mj_footer endRefreshingWithNoMoreData];
                    return;
                }
                NSMutableArray *array = [NSMutableArray arrayWithArray:@[]];
                for (NSDictionary *item in result[@"products"]){
                    [array addObject:[Product getDataFromDictionary:item]];
                }
                if (self.currentPage == INITIAL_PAGE){
                    [self.productArray removeAllObjects];
                }
                [self.productArray addObjectsFromArray:array];
                self.currentPage += 1;
                [self.tableView reloadData];
            }
            else{
                NSLog(@"Failed to load, omit");
            }
            [self.tableView.mj_header endRefreshing];
            [self.tableView.mj_footer endRefreshing];
        });
    });
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


#pragma mark - ----- Tableview source -----
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return section == 0 ? 1 : [self.productArray count];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, 36)];
    [view setBackgroundColor:[UIColor clearColor]];

    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(16, 10, ScreenWidth, 18)];

    [titleLabel setText:@[@"Collection Info", @"Products"][section]];
    [titleLabel setTextColor:[UIColor grayColor]];
    [titleLabel setFont:[UIFont systemFontOfSize:14]];
    [view addSubview:titleLabel];
    
    return view;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 36;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *simpleTableIdentifier = @"ProductCell";
    ProductCell *cell = (ProductCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil)
    {
        NSArray *nibArray = [[NSBundle mainBundle] loadNibNamed:@"ProductCell" owner:self options:nil];
        cell = [nibArray objectAtIndex:0];
    }

    if (indexPath.section == 0){
        [cell setCellData:self.selectedCollection.imageUrl title:self.selectedCollection.title subtitle:self.selectedCollection.bodyHTML];
    }
    else{
        Product *product = self.productArray[indexPath.row];
        NSString *inventoryString = [NSString stringWithFormat:@"Current inventory: %ld", product.totalInventory];
        [cell setCellData:product.imageURL title:product.title subtitle:inventoryString];
    }
    
    return cell;
}


- (UITableView *)tableView
{
    if (!_tableView) {
        CGFloat tableX = 0;
        CGFloat tableY = 0;
        CGFloat tableW = ScreenWidth;
        CGFloat tableH = ScreenHeight - tableY;
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(tableX, tableY, tableW, tableH) style:UITableViewStyleGrouped];
        [_tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"UITableViewCell"];
        _tableView.estimatedRowHeight = 70;
        _tableView.delegate = self;
        _tableView.dataSource = self;
    }
    return _tableView;
}
@end
