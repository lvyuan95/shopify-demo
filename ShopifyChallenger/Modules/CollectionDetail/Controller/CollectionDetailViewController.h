//
//  CollectionDetailViewController.h
//  ShopifyChallenger
//
//  Created by Khalil on 2019/1/18.
//  Copyright © 2019年 Khalil. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Collection.h"

@interface CollectionDetailViewController : UIViewController

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) Collection *selectedCollection;

@end
